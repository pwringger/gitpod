// Copyright (c) 2021 Gitpod GmbH. All rights reserved.
// Licensed under the GNU Affero General Public License (AGPL).
// See License-AGPL.txt in the project root for license information.

package ide

const (
	CodeIDEImage                = "ide/code"
	CodeIDEImageStableVersion   = "commit-f17f584e2e252f3c1e877d28ea1edd1ab137a831" // stable version that will be updated manually on demand
	CodeDesktopIDEImage         = "ide/code-desktop"
	CodeDesktopInsidersIDEImage = "ide/code-desktop-insiders"
	IntelliJDesktopIDEImage     = "ide/intellij"
	GoLandDesktopIdeImage       = "ide/goland"
	PyCharmDesktopIdeImage      = "ide/pycharm"
	PhpStormDesktopIdeImage     = "ide/phpstorm"
	RubyMineDesktopIdeImage     = "ide/rubymine"
	WebStormDesktopIdeImage     = "ide/webstorm"
	JetBrainsBackendPluginImage = "ide/jb-backend-plugin"
)
